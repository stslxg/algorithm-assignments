\documentclass[12pt,a4paper]{article}
%\usepackage{ctex}
\usepackage{amsmath,amscd,amsbsy,amssymb,latexsym,url,bm,amsthm}
\usepackage{epsfig,graphicx,subfigure}
\usepackage{enumitem,balance}%,mathtools}
\usepackage{wrapfig}
\usepackage{mathrsfs, euscript}
\usepackage[usenames]{xcolor}
\usepackage{hyperref}
%\usepackage{algorithm}
%\usepackage{algorithmic}
\usepackage[vlined,ruled,commentsnumbered,linesnumbered]{algorithm2e}
%\usepackage[ruled,lined,boxed,linesnumbered]{algorithm2e}
\usepackage{tikz}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{exercise}{Exercise}[section]
\newtheorem*{solution}{Solution}

\renewcommand{\thefootnote}{\fnsymbol{footnote}}

\newcommand{\postscript}[2]
 {\setlength{\epsfxsize}{#2\hsize}
  \centerline{\epsfbox{#1}}}

\renewcommand{\baselinestretch}{1.0}

\setlength{\oddsidemargin}{-0.365in}
\setlength{\evensidemargin}{-0.365in}
\setlength{\topmargin}{-0.3in}
\setlength{\headheight}{0in}
\setlength{\headsep}{0in}
\setlength{\textheight}{10.1in}
\setlength{\textwidth}{7in}
\makeatletter \renewenvironment{proof}[1][Proof] {\par\pushQED{\qed}\normalfont\topsep6\p@\@plus6\p@\relax\trivlist\item[\hskip\labelsep\bfseries#1\@addpunct{.}]\ignorespaces}{\popQED\endtrivlist\@endpefalse} \makeatother
\makeatletter
\renewenvironment{solution}[1][Solution] {\par\pushQED{\qed}\normalfont\topsep6\p@\@plus6\p@\relax\trivlist\item[\hskip\labelsep\bfseries#1\@addpunct{.}]\ignorespaces}{\popQED\endtrivlist\@endpefalse} \makeatother
\begin{document}
\noindent

%========================================================================
\noindent\framebox[\linewidth]{\shortstack[c]{
\Large{\textbf{Lab07-Graph Algorithms II}}\vspace{1mm}\\
Exercises for Algorithms by Xiaofeng Gao, 2015 Spring Semester}}
\begin{center}
\footnotesize{\color{blue} \quad Name: Xuangui Huang  \quad Student ID: 1140339025 \quad Email: stslxg@gmail.com}
\end{center}


\begin{enumerate}
\item When processing the DFS for an undirected graph $G=(V,E)$, we can define three types of edges for each $(v,u) \in E$ as follows:
\begin{enumerate}
\item \emph{tree edges:} $(v,u)$ is an edge of the resulting DFS tree.
\item \emph{vertical edges:} for an edge $(v,u)$, $v$ is either an ancestor or a descendent of $u$ in the DFS tree (but not the direct parent or child).
\item \emph{crossing edges:} all edges which are neither tree edges nor vertical edges.
\end{enumerate}

Now please modify the pseudo codes of DFS in Alg.~\ref{Alg-DFS} and Alg.~\ref{Alg-Explore}, such that we can label the type of each edge when traveling $G$ by DFS. Will you label a \emph{crossing edge} during the executing process of your modified DFS?

\begin{solution}
\textcolor{white}{ }

\begin{algorithm}[H]
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\caption{\textsc{DFS}($G$)} \label{Alg-DFS}
\BlankLine
\For{all $v \in V$}{visited($v$)=false\;}
\For{all $v \in V$}{
\If{not visited($v$)}{\textsc{Explore}($G,v, \varnothing$)\;}}
\For{all $e \in E$ that is not marked}{Mark $e$ as \emph{crossing edge}\;}
\end{algorithm}

\begin{algorithm}[H]
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\caption{\textsc{Explore}$(G,v,P)$} \label{Alg-Explore}
\Input{$G=(V,E)$ is a graph; $v\in V$; $P$ is the set of vertices on the current DFS path}
\Output{visited($u$) is set to true for all nodes $u$  \textcolor{red}{reachable} from $v$}
%\BlankLine
visited($v$) = true\;
\For{each edge $(v,u)\in E$}{
\If{not visited($u$)}{
	Mark $(v,u)$ as \emph{tree edge}\; 
	\textsc{Explore}($G,u, P\cup \{v\}$)\;}
\Else{
	\If{$u \in P$}{
	Mark $(v,u)$ as \emph{vertical edge}\;}
}
}
\end{algorithm}

No, in my algorithm croosing edges are not labeled in the DFS. But it can be further modified to a version that also labels crossing edges during DFS, just by adding a parameter denoting its direct father in the DFS tree to tell crossing edges from tree edges.
\end{solution}

\item
Professor F. Lake suggests the following algorithm for finding the shortest path from node $s$ to node $t$ in a directed graph with some negative edges: add a large constant to each edge weight so that all the weights become positive, then run Dijkstra's algorithm starting at node $s$, and return the shortest path found to node $t$.

Is this a valid method? Either prove that it works correctly, or give a counterexample.
\begin{solution}
It is not valid since the number of edges in a simple path from $s$ to $t$ may be different. Consider the following example, where $s$ is $A$ and $t$ is $D$:


\begin{tikzpicture}
{\tikzstyle{every node} = [draw, shape = circle]
\node (a) at (0,0) {A};
\node (b) at (1,1) {B};
\node (c) at (3,1) {C};
\node (d) at (4,0) {D};
\node (e) at (2, -1) {E};}
\draw[->] (a) -- (b) node[pos=.5,sloped,above] {$0.5$};
\draw[->] (b) -- (c) node[pos=.5,sloped,above] {$1$};
\draw[->] (c) -- (d) node[pos=.5,sloped,above] {$0.5$};
\draw[->] (a) -- (e) node[pos=.5,sloped,above] {$5$};
\draw[->] (e) -- (d) node[pos=.5,sloped,above] {$-2$};
\end{tikzpicture}
Then $weight(path(A, B, C, D)) = 2$ and  $weight(path(A, E, D)) = 3$ hence $A \rightarrow B \rightarrow C \rightarrow D$ is the shortest path. However using Lake's method, we have to add each edges by a constant $\epsilon > 2$, hence  $weight(path(A, B, C, D)) = 2 + 3\epsilon$ and $weight(path(A, E, D)) = 3 + 2\epsilon$. For $\epsilon > 2$ we have $2 + 3\epsilon > 3 + 2\epsilon$ thus we will choose the wrong path. 
 
\end{solution}

\item
Consider a directed graph in which the only negative edges are those that leave $s$; all other edges are positive. Can Dijkstra's algorithm, started as $s$, fail on such a graph? Prove your answer.
\begin{solution}
No, Dijkstra's algorithm won't fail on such a graph. The crucial point here is that for any vertex $y$ on a shortest path from $s$ to any vertex $u$, other than $s$ and $u$, we have $\sigma(s,y) \leq \sigma(s,u)$. Hence all the proofs remain valid, i.e. we can prove $d(v) \geq \sigma(s,v)$ for any vertex $v$ at any stage and $d(v) = \sigma(s,v)$ whenever $v$ is added into $S$.  
\end{solution}

\item
Let $G=(V, E)$ be a connected, undirected graph. Give an $O(V+E)$-time algorithm to compute a path in $G$ that traverses each edge in $E$ exactly once in each direction. Describe how you can find your way out of a maze if you are given a large supply of pennies.
\begin{solution}
The algorithm is very similar to the first DFS algortihm. W.l.o.g. assume the graph is simple.
\begin{algorithm}[H]
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\caption{\textsc{DFS}($G$)}
\BlankLine
\For{all $v \in V$}{visited($v$)=false\;}
\For{all $v \in V$}{
\If{not visited($v$)}{\textsc{Explore}($G,v, \varnothing$)\;}}
\end{algorithm}

\begin{algorithm}[H]
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\caption{\textsc{Explore}$(G,v,P)$}
\Input{$G=(V,E)$ is a graph; $v\in V$; $P$ is the set of vertices on the current DFS path}
\Output{visited($u$) is set to true for all nodes $u$  \textcolor{red}{reachable} from $v$}
%\BlankLine
visited($v$) = true\;
\For{each edge $(v,u)\in E$}{
\If{not visited($u$)}{
	Output $(v,u)$\; 
	\textsc{Explore}($G,u, P\cup \{v\}$)\;
	Output $(u,v)$\;
}\Else{
	\If{$u \in P$}{
	Output $(v,u)$, $(u,v)$\;
}}
}
\end{algorithm}
To get out of a maze just simply following the wall on your right hand side.
\end{solution}

\end{enumerate}
%========================================================================
\end{document}
