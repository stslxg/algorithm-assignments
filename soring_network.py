c = 0.08
def draw2(i, j, ii, jj, flag):
    if flag:
        print "\\draw[style=thick] (", j,",", i,") -- (", jj, ",", ii,");"
    else:
        print "\\draw (", j,",", i,") -- (", jj, ",", ii,");"

def draw(i, j, ii, jj):
    draw2(i, j, ii, jj, True)
    print "\\draw[fill=black] (", j,",", i,") circle (",c,");"
    print "\\draw[fill=black] (", jj, ",", ii,") circle (", c, ");"

def sorter(i, j, n):
    if n == 2 :
        draw(i, j, i+1, j)
        return 1
    else:
        res = sorter(i, j, n/2)
        sorter(i+n/2, j, n/2)
        res += merger(i, j+res, n)
        return res

def merger(i, j, n):
    for k in range(0, n/2):
        draw(i+k, j+k, i+n-k-1, j+k)

    res = bitsorter(i, j+n/2, n/2)
    bitsorter(i+n/2, j+n/2, n/2)
    return res + n/2

def bitsorter(i, j, n):
    if n == 1:
        return 0
    else:
        for k in range(0, n/2):
            draw(i+k, j+n/2-k-1, i+k+n/2, j+n/2-k-1)

        res = bitsorter(i, j+n/2, n/2)
        bitsorter(i+n/2, j+n/2, n/2)
        return res + n/2

def sorting_network(n):
    res = sorter(1,1,n)
    for k in range(0, n):
        draw2(1+k, 0, 1+k, res+1, False)

sorting_network(16)
