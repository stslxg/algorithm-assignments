\documentclass{scrartcl}
\usepackage[headsepline, footsepline, markuppercase]{scrpage2}

\rohead{L. Deng, Q. He, X. Huang}
\lohead{\bfseries Cooperative Sweep Coverage Problem}
\chead{}

\pagestyle{scrheadings}

\usepackage{authblk}
%\usepackage{beton}
\usepackage{euler}

\usepackage{float}
\usepackage{subfigure}
\usepackage{verbatim}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{cmap}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}
\usepackage{tikz}
\usepackage[ruled,lined,boxed,linesnumbered]{algorithm2e}

\newcommand{\prob}[3]{
\begin{center}
  \normalfont\fbox{
   \begin{tabular}[!htbp]{rp{9cm}}
   	\textsc{#1} & \\
   	\textit{Instance:}&#2. \\
     \textit{Problem:}&#3
   \end{tabular}}
\end{center}}


\newtheorem{Thm}{Theorem}
\newtheorem{Def}{Definition}
\newtheorem{Fact}[Thm]{Fact}
\newtheorem{Cor}[Thm]{Corollary}
\newtheorem{Conj}[Thm]{Conjecture}
\newtheorem{Lem}[Thm]{Lemma}
\newtheorem{Prop}[Thm]{Proposition}
\newtheorem{Prob}{Problem}
\newtheorem{Exam}{Example}
\newtheorem{Rem}{Remark}
\newtheorem{Not}[Thm]{Notation}
\newtheorem{assumption}{Assumption}

% MATH -----------------------------------------------------------
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\Real}{\mathbb R}
\newcommand{\eps}{\varepsilon}
\newcommand{\To}{\longrightarrow}
\newcommand{\BX}{\mathbf{B}(X)}
\newcommand{\A}{\mathcal{A}}
\newcommand{\CommentS}[1]{}

\newcommand{\la}{\leftarrow}
\newcommand{\La}{\Leftarrow}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\cP}{\cal{P}}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}


\title{Cooperative Sweep Coverage Problem}
\subtitle{Project Report for Algorithms: Analysis and Theory}
\author{Licong Deng\thanks{ID: 1140339017, email: \texttt{denglicong@sjtu.edu.cn}}}

\author{Qiyuan He\thanks{ID: 1140339002, email: \texttt{qiyuanhe56@163.com}}}

\author{Xuangui Huang\thanks{ID: 1140339025, email: \texttt{stslxg@gmail.com}}}
\affil{Shanghai Jiao Tong University}

\date{\today}

\begin{document}
\maketitle
\tableofcontents

\section{Problem Formulation}
The Cooperative Sweep Coverage problem (CSC) is defined as an optimization problem. Assuming we are given $n$ targets located at point $p_1$, $\dots$, $p_n$ and $m$ mobile sensors, we first define the concept of ``feasible solution'':
\begin{Def}[Trajectory Schedule]
We say a tuple $\sigma = (k, \gamma_1, \dots, \gamma_k, b_1, \dots, b_k)$ is a \emph{trajectory schedule} for mobile sensors if the following requirements are satisfied:
\begin{itemize}
	\item $\gamma_1$, $\dots$, $\gamma_k$ are cycles such that each of the $n$ target points is covered exactly once by them;
	\item $b_i \ge 1$ for all $1 \le i \le k$ and $\sum_{i = 1}^k b_i = m$.
\end{itemize}
\end{Def}
Note that implicitly we have $k \le m$. Now we can define the cost of a trajectory schedule:
\begin{Def}[Cost]
For a trajectory schedule $\sigma = (k, \gamma_1, \dots, \gamma_k, b_1, \dots, b_k)$, we define its cost $cost(\sigma) \triangleq \max_{1 \le i \le k} \frac{len(\gamma_i)}{b_i}$, where $len(\gamma)$ denotes the length of the cycle $\gamma$.
\end{Def}
Then the Cooperative Sweep Coverage problem is defined as follow:
\prob{CSC}{$n$ targets located at point $p_1$, $\dots$, $p_n$ and $m$ mobile sensors}{Find a trajectory schedule $\sigma$ that minimize $cost(\sigma)$}

Note that in this problem, we want to get $k$ Hamiltonian cycles for some $k$ with $1 \leq k \leq m$ and then assign $m$ mobile sensors to them to minimize our goal. Therefore we can divide this problem into two steps:
\begin{enumerate}
    \item[] \emph{Step 1}: obtain $k$ Hamiltonian cycles to cover all the vertices;
    \item[] \emph{Step 2}: assign $m$ sensors to the $k$ cycles as evenly as possible.
\end{enumerate}

This two steps' view on the problem plays a crucial role throughout our work.

\subsection{Notations and Symbol Table}
In this report, $[n]$ denotes the set $\{1, \dots, n\}$. Table \ref{lab:sym} summarizes almost all the symbols we will use.

\begin{table}[h]
\centering
\begin{tabular}{c|c}
Notation & Meaning \\ \hline \hline
$p_1, \dots, p_n$ & points on $\mathbb{R}$ or $\mathbb{R}^2$ \\ \hline
$P_1, \dots, P_k$ & sets of points \\ \hline
$\gamma$ & a cycle represented as $(v_1, \dots, v_l)$ \\ \hline
$\gamma_1, \dots, \gamma_n$ & disjoint cycles \\ \hline
$b_1, \dots, b_k$ & sequence of integers representing assignment of sensors to cycles \\ \hline
$x_1, \dots, x_n$ & points on $\mathbb{R}$ \\ \hline
$\delta(x_i, x_j)$ & Euclidean distance of points $x_i$ and $x_j$, \\ 
& $= |x_i - x_j|$ \\ \hline
$\delta(p_i, p_j)$ & Euclidean distance of points $p_i$ and $p_j$ on $\mathbb{R}^2$, \\
& $ = \sqrt{(x - x')^2 + (y-y')^2}$ where $p_i = (x,y)$ and $p_j = (x',y')$ \\ \hline
$G$ & a complete graph with $G = (V, E)$ where $V$ contains all the points \\
& and $E = V \times V$ with edge weights $w(p_i, p_j) = \delta(p_i, p_j)$ \\ \hline
$T$ & a tree which is a subgraph of $G$ \\ \hline
$\mathcal{T}$ & a forest with trees which are subgraphs of $G$ 
\end{tabular}
\caption{Symbol Table}\label{lab:sym}
\end{table}

\subsection{Towards Integer Linear Programming}
According to our two steps' view, we try to represent this Cooperative Sweep Coverage problem as an Integer Linear Programming problem.
\subsubsection{Representation of Cycle Covers}
In the first step we want to obtain a cycle cover, thus we must represent a cycle cover in terms of Integer Linear Programming. There are at most $m$ cycles, each of which contains at most $n$ vertices. Therefore we can define auxiliary variables $p_{i,j}$, which are just for convenient of our discussion and won't appear in the programming, and indicator variables $p_{i,j,k}$.
\begin{Def}[Vertex Variables]
For $1 \le i \le m$ and $1 \le j \le n+1$, 
\begin{itemize}
	\item define $p_{i,j}$ to be the variable denoting the index of the $j$-th vertex in the $i$-th cycle;
	\item define $p_{i,j,k}$ to be the variable indicating whether $p_{i,j}$ is $k$, for $1 \le k \le n$;
    \item we allow $p_{i,j}$ to be $0$ and say this vertex is a \emph{vanish vertex}, and define $v_{i,j}$ indicating whether $p_{i,j}$ is vanish;
    \item if all the vertices in a cycle are vanish vertices, such cycle is called a \emph{vanish cycle}, and we define $v_i$ indicating whether cycle $i$ is vanish.
\end{itemize}
\end{Def}
Notice that there must be at least one vanish vertex in each cycle since $j$ can be $n+1$ while a cycle contains at most $n$ vertex. The ``vanish'' concept is introduced since we may have different number of cycles in different cycle covers and the cycles in each cycle cover may contain different numbers of vertices.
\begin{Exam}
Given $n = 4$ and $m = 2$, a cycle $(2, 4, 1)$ can be represent as follow assuming it is the first cycle.
\begin{table}[h]
\centering
\begin{tabular}[c]{c|c|c|c|c}
$p_{1,1}$ & $p_{1,2}$ & $p_{1,3}$ & $p_{1,4}$ & $p_{1,5}$ \\
\hline
$2$ & $4$ & $1$ & $0$ & $0$
\end{tabular}
\begin{tabular}[c]{c|c|c|c|c|c}
 $k$ & $p_{1,1,k}$ & $p_{1,2,k}$ & $p_{1,3,k}$ & $p_{1,4,k}$ & $p_{1,5,k}$ \\ \hline
$1$ & $0$ & $0$ & $1$ & $0$ & $0$\\ \hline
$2$ & $1$ & $0$ & $0$ & $0$ & $0$\\ \hline
$3$ & $0$ & $0$ & $0$ & $0$ & $0$\\ \hline
$4$ & $0$ & $1$ & $0$ & $0$ & $0$
\end{tabular}
\caption{Representation of the cycle using $p_{i,j}$'s and $p_{i,j,k}$'s}
\end{table}
\end{Exam}
\begin{Fact}
$p_{i,j} = \sum_{k = 1}^{n} k \cdot p_{i,j,k}$.
\end{Fact}
\begin{Prop}
We can constrain all the vertex variables $p_{i,j,k}$'s using linear inequalities so that they satisfy the following properties:
\begin{enumerate}
	\item \label{ilp:req:ind} $p_{i,j,k}$ are indicator variables;
	\item \label{ilp:req:one} each vertex is covered exactly once;
	\item \label{ilp:req:var} vanish vertices are the last ones in each cycle, i.e. whenever $p_{i,j} = 0$ for some $j$, we have $p_{i,j'} = 0$ for all $j \le j' \le n+1$. 
\end{enumerate}
Moreover we can constrain $v_{i,j}$'s and $v_i$'s so that they represent what we want.
\end{Prop}
\begin{proof}
For requirement \ref{ilp:req:ind} we just use the following inequalities:
\begin{align}\label{ilp:1}
p_{i,j,k} \ge 0, p_{i,j,k} \le 1,  \text{ }\forall 1 \le i \le m, 1 \le j \le n+1, 1 \le k \le n
\end{align}
\begin{align}\label{ilp:7}
\sum_{k = 1}^n p_{i,j,k} \le 1, \text{ }\forall 1 \le i \le m, 1 \le j \le n+1
\end{align}

For requirement \ref{ilp:req:one} we can simply use the following ones:
\begin{align}\label{ilp:2}
\sum_{1 \le i \le m, 1 \le j \le n+1} p_{i,j,k} = 1, \text{ }\forall 1 \le k \le n
\end{align}
Note that we should use two inequalities to represent one equality, but for simplicity here we just write down the equalities throughout this section.

Now to write down constraints on $v_{i,j}$'s is easy:
\begin{align}\label{ilp:6}
\sum_{k = 1}^n p_{i,j,k} + v_{i,j} = 1, \text{  } \forall 1\le i \le m, 1 \le j \le n+1
\end{align}
$v_{i,j}$ is thus implicitly $\{0,1\}$-valued by constraints (\ref{ilp:1}) and (\ref{ilp:7}).

For the last requirement, note that now we have indicator variables for vanish vertices. Hence the following constraints are sufficient:
\begin{align}\label{ilp:3}
v_{i,j} - v_{i, j+1} \le 0, \text{  } \forall 1\le i \le m, 1 \le j \le n
\end{align}

By this property $v_i$ is just $v_{i,1}$:
\begin{align}\label{ilp:8}
v_{i} - v_{i, 1} = 0, \text{  } \forall 1\le i \le m
\end{align}
\end{proof}

\subsubsection{Calculating the Length of Cycles}
Unfortunately calculating the length of each cycle is a little bit involved because they may contain different numbers of vertices and it's quite difficult to find the last vertex in each cycle.
\begin{Def}
For $1 \le i \le m$, define $c_i$ to be the variable representing the weight of the $i$-th cycle.
\end{Def}
\begin{Prop}
We can constraint $c_i$'s using linear inequalities such that their values are exactly the weights of cycles.
\end{Prop}
\begin{proof}
First we introduce variables $p_{i,j,k,k'}$ indicating whether point $p_{i,j} = k$ and $p_{i, j+1} = k'$ for $1 \le i \le m$ and $1 \le j \le n$. Notice that $p_{i,j,k} + p_{i,j+1,k'}$ can only take values in $\{0, 1, 2\}$ hence we can use it to get value of $p_{i,j,k,k'}$ using linear inequalities:
\begin{align}\label{ilp:9}
p_{i,j,k, k'} \ge 0,  p_{i,j,k,k'} - \frac{1}{2}(p_{i,j,k} + p_{i,j+1,k'}) \le 0, (p_{i,j,k} + p_{i, j+1, k'}) - 1 - p_{i,j,k,k'} \le 0 \notag \\
\text{ }\forall 1 \le i \le m, 1 \le j \le n+1, 1 \le k \le n, 1 \le k' \le n
\end{align}
Note that $p_{i,j,k,k'}$ will be $0$ for all $k$ and $k'$ if $p_{i,j}$ or $p_{i,j+1}$ is vanish.

Then we introduce variables $d_{i,j}$ representing distance between point $p_{i,j}$ and $p_{i, j+1}$ using the following constraints:
\begin{align}\label{ilp:10}
 \sum_{1 \le k \le n, 1 \le k' \le n}\delta(p_k, p_{k'})p_{i,j,k,k'}  - d_{i,j} = 0, \text{ }\forall 1 \le i \le m, 1 \le j \le n
\end{align}
where coefficients $\delta(p_k, p_{k'})$ are all constants given the coordinates of target points. Note that $d_{i,j} = 0$ if $p_{i,j}$ or $p_{i,j+1}$ is vanish.

We still have to represent the distance between the last point and the first point of the cycle. Thus we introduce variable $l_{i,j}$ indicating whether $p_{i,j}$ is the last non-vanish point of cycle $i$:
\begin{align}\label{ilp:11}
v_{i, j+1} - v_{i, j} - l_{i,j} = 0, \text{ }\forall 1 \le i \le m, 1 \le j \le n
\end{align}
since the only possible values for $(v_{i,j}, v_{i,j+1})$ are $(0,1)$, $(0,0)$ and $(1,1)$ by constraints (\ref{ilp:3}) and in exactly the first case $l_{i,j}$ should be $1$.

We introduce variables $l_{i,j,k}$ indicating whether the $p_{i,j} = k$ is the last non-vanish point. Then similar to (\ref{ilp:9}), which is indeed a general method to calculate the \emph{product} of two indicator variables using their linear combinations, we can set the following constraints:
\begin{align}\label{ilp:13}
l_{i,j,k} \ge 0,  l_{i,j,k} - \frac{1}{2}(p_{i,j,k} + l_{i,j}) \le 0, (p_{i,j,k} + l_{i,j}) - 1 - l_{i,j,k} \le 0 \notag \\
\text{ }\forall 1 \le i \le m, 1 \le j \le n, 1 \le k \le n
\end{align}
Then we introduce variable $l_{i,j,k, k'}$ indicating whether $l_{i,j,k} = 1$ and $p_{i,1} = k'$ again by the same method:
\begin{align}\label{ilp:14}
l_{i,j,k, k'} \ge 0,  l_{i,j,k, k'} - \frac{1}{2}(p_{i,1,k'} + l_{i,j,k}) \le 0, (p_{i,1,k'} + l_{i,j,k}) - 1 - l_{i,j,k,k'} \le 0 \notag \\
\text{ }\forall 1 \le i \le m, 1 \le j \le n, 1 \le k \le n, 1 \le k' \le n
\end{align}

Finally we can represent the weight of cycle $i$ using linear inequalities:
\begin{align}\label{ilp:12}
\sum_{j = 1}^n d_{i,j} + \sum_{1 \le j \le n, 1 \le k \le n, 1 \le k' \le n}\delta(p_k, p_{k'})l_{i,j,k,k'} - c_i = 0, \text{ }\forall 1 \le i \le m
\end{align}
Note that $c_i = 0$ if cycle $i$ is vanish.
\end{proof}


\subsubsection{Representation of Sensor Partitions}
\begin{Def}
For $1 \le i \le m$, define $b_i$ to be the variable representing the number of mobile sensors assigned to the $i$-th cycle.
\end{Def}

\begin{Prop}
We can constraint $b_i$'s to have the following properties:
\begin{enumerate}
	\item If the $i$-th cycle is vanish then $b_i \ge 0$, otherwise $b_i \ge 1$;
	\item $\sum_{i=1}^m b_i = m$. 
\end{enumerate}
\end{Prop}
\begin{proof}
Just use the following constraints:
\begin{align}\label{ilp:5}
	b_i + v_i \ge 1, \text{   	} \forall 1 \le i \le m 
\end{align}
\begin{align}\label{ilp:4}
\sum_{i =1}^m b_i = m.
\end{align}
\end{proof}

\begin{Rem}
Notice that allowing sensors on vanish cycles only makes the result worser so we can allow $b_i \ge 0$ instead of $b_i = 0$ for vanish cycle $i$.
\end{Rem}

\subsubsection{Integer Linear Programming?}
It is impossible to use linear functions to represent $\max_{1 \le i \le m} \frac{c_i}{b_i}$: the only way is to add a variable $c$ with constraints $c \ge \frac{c_i}{b_i}$ but then these constraints are not linear. Hence we cannot convert this problem into Integer Linear Programming . However for the following decision version of $\textsc{CSC}$ we can convert it into ILP.

\prob{CSC$_D$}{$n$ targets located at point $p_1$, $\dots$, $p_n$ and $m$ mobile sensors, and a positive real number $c$}{Decide whether there is a trajectory schedule $\sigma$ such that that $cost(\sigma) \le c$}

\begin{Prop}
Given an instance of \textsc{CSC$_D$} we can construct an instance of Integer Linear Programming such that the former is a `yes' instance if and only if the latter one has feasible solutions.
\end{Prop}

\begin{proof}
Notice that $c b_i \ge c_i$ also holds for vanish cycle $i$. Hence we can have the following additional constraints:
\begin{align}
c \cdot b_i - c_i \ge 0, \text{   } \forall 1 \le i \le m
\end{align}
Combine them with all the previous constraints we get the ILP we want.
\end{proof}

\section{CSC in One-Dimensional Space}
When the points are distributed on a line, this problem becomes easier. Indeed we can have a Dynamic Programming algorithm to solve it efficiently. The crucial point to come up with this algorithm is that each point should be covered by exactly one cycle. Given $n$ target points $x_1$, $x_2$, $\dots$, $x_n$ on a line in increasing order and $m$ mobile sensors, the problem to cover the first $i$ points with the first $j$ sensors can be reduced into a series of subproblems to cover the first $k$ points with the first $l$ sensors then use the remaining $j-l$ sensors to cover the remaining points, for all $1\leq k \leq i-1$ and $1 \leq l \leq j-1$. Hence if we denote the minimized value of the longest trajectory length restricted to the first $i$ points and $j$ sensors as $f[i, j]$, then we have the following formula:
\[f[i,j] = \min \{\frac{2\delta(x_i, x_1)}{j}, \min_{1\leq k \leq i-1, 1 \leq l \leq j-1} \max \{f[k, l], \frac{2\delta(x_i, x_{k+1})}{j-l}\}\}.\]

\subsection{Algorithm and Analysis}
Therefore the algorithm \textsc{1-D CSC} is straightforward. 

\begin{algorithm}[h] \label{alg:1d}
\BlankLine
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\SetKwComment{tcp}{}{}
\caption{\textsc{1-D CSC}}
\Input{An integer $n$, and $n$ target points $x_1$, $x_2$, $\dots$, $x_n$ on a line in increasing order, and an integer $m$}
\Output{The minimized value of the longest trajectory length $f[n, m]$}
\BlankLine
Initialize elements of $f$ to be all $0$\;
\For{$i \la 2$ \KwTo $n$}{
    \For{$j \la 1$ \KwTo $m$}{
        $f[i, j] \la 2 (x_i - x_1) \slash j$\;
        \For{$k \la 1$ \KwTo $i-1$}{
            \For{$l \la 1$ \KwTo $j-1$}{
                $f[i, j] \la \min\{f[i, j], \max\{f[k, l], 2(x_i - x_{k+1}) \slash (j-l)\}\}$\; \label{alg:1d:mod}
            }
        }
}}
Output $f[n, m]$\;
\end{algorithm}

Observe that we have four nested \texttt{for}-loops in this algorithm, hence its running time is 
\[\sum_{i = 2}^n \sum_{j = 1}^m (i-1)(j-1) = O(n^2m^2).\]
Besides, we can prove the correctness of our algorithm as follows:

\begin{Thm}
The algorithm \textsc{1-D CSC} will always output the optimum value for \textsc{CSC} in one-dimensional space.
\end{Thm}
\begin{proof}
For the sake of contradiction, we choose $(i,j)$ to be the first pair lexicographically such that there is an instance with $i$ points and $j$ sensors and the optimum value is $c \neq f[i,j]$. Such $(i,j)$ must exist by the assumption we make, otherwise we will always output the optimum value.

We know that the $i$-th point is exactly at the end of some cycle in the optimum strategy since it is the last point. Hence we have the following two cases:
\begin{itemize}
	\item if in the optimum strategy we cover point $1$ to point $i$ with a single cycle and $j$ sensors, then $c = \frac{2\delta(x_i, x_1)}{j} \ge f[i, j]$ by our definition of $f$;
	\item otherwise in the optimum strategy the $i$-th point must be covered by a certain cycle starting from point $k+1$ with $j-l$ sensors assigned with $1 \le k \le i-1$ and $1 \le l \le j-1$. Then consider the subproblem for $(k, l)$, since it is lexicographically smaller than $(i,j)$, the cost $c'$ from the above optimum strategy restricted to the subproblem to cover the first $k$ points with $l$ sensors is not smaller then $f[k, l]$. Therefore $c = \max \{c', \frac{2\delta(x_i, x_{k+1})}{j-l}\} \ge \max \{f[k, l], \frac{2\delta(x_i, x_{k+1})}{j-l}\} \ge f[i, j]$ by the monotonicity of $\max$ function and our definition of $f$.
\end{itemize}

Therefore we must have $c > f[i, j]$, contradicting to the optimality of the optimum strategy.
\end{proof}

\subsection{Algorithm that Outputs the Best Strategy}

We can modify the above algorithm with some auxiliary information to recover the best strategy, which is represented as a sequence of tuples $(b_i, l_i, r_i)_{i \in [k]}$ where we have $k$ cycles and the $i$-th cycle uses $b_i$ sensors to cover point $x_{l_i}$ to point $x_{r_i}$.

The modification is quite straightforward and its time complexity remains the same: 
\begin{enumerate}
	\item add two auxiliary arrays $b$ and $l$, initialize $b[i,j]$ to be $j$ and $l[i,j]$ to be $1$ for all $1 \le i \le n$ and $1 \le j \le m$;
	\item on Line \ref{alg:1d:mod} of Algorithm \textsc{1-D CSC}, whenever $f[i,j]$ is updated into a smaller value we record the corresponding $j-l$ as $b[i,j]$ and $k+1$ as $l[i,j]$;
	\item at last we use the following algorithm to recover the strategy:
\end{enumerate}

\begin{algorithm}[h]
\BlankLine
\caption{\textsc{1-D CSC ModOutput}}
\BlankLine
$i \la n$, $j \la m$\;
\While{$i > 0$}{
	Output $(b[i,j], l[i,j], i)$\;
	$tmpj \la j$\;
	$j \la j - b[i,j]$\;
	$i \la l[i,tmpj] - 1$\;
}
\end{algorithm}

\subsection{Illustration and Comparisons}
Following is an Illustration of our algorithm with $n = 20$ and $m = 3$.

\begin{figure}[H]
\centering
\includegraphics[scale=0.45]{1D-result/ill.PNG}
\caption{Output strtegy of modified version of \textsc{1-D CSC} with $n = 20$ and $m = 3$}
\end{figure}

We use the following two baselines:
\begin{enumerate}
	\item[\emph{1}] output $\frac{\delta(x_n - x_1)}{m}$;
	\item[\emph{2}] partition the target points into $m$ clusters using $k$-means clustering\cite{John}, then output $\max_{1\le i \le m} 2\delta(x_{r_i} - x_{l_i})$ for each segment $(l_i, r_i)$.
\end{enumerate}
Implementation of Baseline 1 is \verb!1D-code/baseline1_1D.cpp!, and of Baseline 2 is \\ \verb!1D-code/baseline2_1D.cpp!. Implementation of our algorithm is \verb!1D-code/1D_algorithm.cpp!.

We test the our algorithm with these two baselines, using points randomly generated on grid $[1, p] \times [1, p]$ with $p$ ranges from $50$ to $1000$. Results are enclosed in file \verb!1D-result/1D-result.xlsx!, here we present several figures to show that our algorithm really outputs better result than two baselines'.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{"1D-result/a.PNG"}
\includegraphics[scale=0.6]{"1D-result/b.PNG"}
\includegraphics[scale=0.6]{"1D-result/c.PNG"}
\includegraphics[scale=0.6]{"1D-result/d.PNG"}
\caption{Results of our algorithm and two baselines, each data point is taken average on $50$ instances}
\end{figure}

\section{CSC in Two-Dimensional Space}

\subsection{Optimal algorithm for Small-Scale Instance}
Under a simple observation we can solve Step 2 efficiently, whose algorithm will also be used by the approximation algorithm. For small-scale instance, we can just enumerate all cycle covers brute-forcely, then use the following exact algorithm to calculate the optimal cost of each cycle cover, and choose the minimal one. 

\subsubsection{Exact Algorithm for Step 2}

Note that the second step can be done exactly in polynomial time: given $k$ cycles of weights $c_1$, $c_2$, $\dots$, $c_k$, it is equivalent to the following integer optimization problem:

\[\begin{array}{rcc} 
    \textbf{minimize} & \max_{1\le i \le k} \frac{c_i}{b_i}& \tag{*} \\
    \textbf{subject to}
     & b_i \ge 1, & \forall 1 \le i \le k \\
     & \sum_{i = 1}^k b_i = m,&
\end{array}\] 
where $b_i$ denotes the number of mobile sensors assigned to the $i$-th cycle.

Similarly as the algorithm for \textsc{CSC} problem in $1$-dimensional space, we can have the following Dynamic Programming algorithm \textsc{SensorPartition} to partition our sensors and its correctness is proved as follows.

\begin{algorithm}[h] \label{alg:senpar}
\BlankLine
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\SetKwComment{tcp}{}{}
\caption{\textsc{SensorPartition($k$, $c_1$, $\dots$, $c_k$, $m$)}}
\Input{An integer $k$, and $k$ cycle weights $c_1$, $\dots$, $c_k$, and an integer $m$}
\Output{The minimized value of (*) and an optimal solution $b_1$, $\dots$, $b_k$}
\BlankLine
Initialize $g[0, j]$ to be $0$ for $0 \le j \le m$\;
Initialize all other elements of $g$ to be $\infty$\; 
\For{$i \la 1$ \KwTo $k$}{
    \For{$j \la i$ \KwTo $m$}{
        \For{$l \la i-1$ \KwTo $j-1$}{
            $tmp \la \max\{g[i-1, l], \frac{c_i}{j-l}\}\}$\;
            \If{$tmp < g[i, j]$}{
                $g[i, j] \la tmp$\;
                $b[i, j] \la j - l$\;
            }
        }
    }
}
Output $g[k, m]$\;
$j \la m$\;
\For{$i \la k$ \KwTo $1$}{
    Output $b[i,j]$ as $b_i$\;
    $j \la j - b[i,j]$\;
                  
}
\end{algorithm}


\begin{Thm}\label{thm:senpar}
The \textsc{SensorPartition} algorithm will output the optimal value and an optimal solution in time $O(km^2)$.
\end{Thm}

\begin{proof}
The running time of this algorithm is $\sum_{i = 1}^k \sum_{j = i}^m ((j-1)-(i-1)) = O(km^2)$.

For the sake of contradiction, we choose $(i,j)$ to be the first pair lexicographically such that there is an instance with $i$ cycles and $j$ sensors and the optimum value is $c \neq g[i,j]$. Such $(i,j)$ must exist by the assumption we make, otherwise we will always output the optimum value.

In the optimum strategy we must assign $j-l$ sensors to the last cycle for some $i-1 \le l \le j-1$. Then consider the subproblem $(i-1, l)$, since it is lexicographically smaller than $(i,j)$, the cost from the above optimum strategy restricted to the subproblem of assigning $l$ sensors to the first $i-1$ cycles is not smaller than $f[k, l]$. Therefore $c \ge \max \{g[i-1, l], \frac{c_i}{j-l}\} \ge g[i, j]$. Thus $c > g[i,j]$ contradicting to our assumption of its optimality.
\end{proof}

By the above theorem we are assured that our brute-force algorithm for small-scale instances will output the optimum solution.

\subsection{Polynomial-Time Approximation Algorithm}
In spirit of the $2$-approximation algorithm of \textsc{Metric-TSP}\cite{vijay01}, which uses the minimum spanning tree and Euler cycle to approximate the minimum Hamiltonian cycle with distances as edge weights, we come up with an approximation algorithm for \textsc{CSC} according to our two steps' view. Note that we use the same algorithm \textsc{SensorPartition($k$, $c_1$, $\dots$, $c_k$, $m$)} for Step 2 as before.

\subsubsection{Approximation Method for Step 1 and the Combined Algorithm}

In the \textsc{Metric-TSP}'s $2$-approximation algorithm\cite{vijay01}, 
\begin{enumerate}
	\item we first obtain the minimum spanning tree on the complete graph with distances as edge weights;
	\item then we duplicate each edges in the spanning tree, obtaining a graph that surely has Euler cycles;
	\item at last we use the Euler cycle algorithm to get an Euler cycle, and take each vertex into the output Hamiltonian cycle according to the order it first appears in the Euler cycle.
\end{enumerate}
This is a standard method to obtain a Hamiltonian cycle using the minimum spanning tree, where the weight of the cycle is upper bounded by two times the total length of the spanning tree.

The algorithm $\textsc{MetricTSP}(T)$ to convert from a minimum spanning tree to an Hamiltonian cycle can be easily implemented by a Depth First Search on this tree.

\begin{algorithm}[h]
\BlankLine
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\SetKwComment{tcp}{}{}
\SetKwFunction{DFS}{DFS}
\caption{\textsc{MetricTSP}($T$)}
\Input{A minimum spanning tree $T$}
\Output{A Hamiltonian cycle $(v_1, \dots, v_m)$ where all the $v_i$'s are vertices of $T$ and $m$ is the number of vertices of $T$}
\BlankLine
Initialize all elements of $u$ to be \textbf{false}\;
$m \la 0$\;
For a vertex $v$ of $T$, execute \DFS$(v)$\;
\BlankLine
\SetKwProg{proc}{Procedure}{}{end}
\proc{\DFS($v$)}{
\If{$u[v]$ is \textbf{false}}{
	$u[v] \la$ \textbf{true}, $m \la m+1$\;
	Output $v$ as $v_m$\;
	\For{all $w$ with $(v,w) \in T$}{
		\DFS($w$);
	}
}}


\end{algorithm}

Besides, we will also use standard algorithm $\textsc{MST}$ (Prim's or Kruskal's algorithm\cite{clrs09}) to obtain a minimum spanning tree and procedure $\textsc{Weight}(\gamma)$ to calculate the weight of cycle $\gamma$.

Using all the above algorithms, we can design an algorithm to approximate the \textsc{CSC} problem in two-dimensional space:
\begin{itemize}
	\item at each step we will maintain a forest $\mathcal{T}$ which is a tree cover\cite{even03} for the complete graph $G = (V,E)$ where $V$ contains all the points and $E = V \times V$ with edge weights $w(p_i, p_j) = \delta(p_i, p_j)$;
	\item the forest contains only one tree at first, which is the minimum spanning tree of $G$;
	\item this forest represents the strategy we split the graph into cycles: we will use $\textsc{MetricTSP}(T)$ algorithm to get cycle for each tree $T$ in the forest $\mathcal{T}$, and use $\textsc{Weight}$ to calculate their weight and then use the optimum algorithm \textsc{SensorPartition} for Step 2 to obtain a solution thus we get a trajectory schedule $\sigma$ and its cost;
	\item at the end of each step, we want to generate another forest with exactly one more tree: we choose to cut the edges with the \emph{largest} weight in the forest, thus splitting the original tree which contains this edges into two new trees;
	\item we output the trajectory schedule with the minimum cost among those we have generated as our approximation.
\end{itemize}

Therefore \textsc{2-D CSC} is our approximation algorithm, formally describe as Algorithm \ref{alg:2d}.

\begin{algorithm}[h] \label{alg:2d}
\BlankLine
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}
\SetKwComment{tcp}{}{}
\caption{\textsc{2-D CSC}}
\Input{An integer $n$, and $n$ target points $p_1$, $\dots$, $p_n$, and an integer $m$}
\Output{The approximated optimal value and strategy: the cycles $\gamma$'s and their respective number of sensors $b$'s}
\BlankLine
$\mathcal{T} \la \{\textsc{MST}(G)\}$\;
\For{$i \la 1$\KwTo $m$}{
    Let $T_1$, $\dots$, $T_i$ be the trees in $\mathcal{T}$\;
    \For{$j \la 1$ \KwTo $i$}{
        $\gamma_j \la \textsc{MetricTSP}(T_j)$\;
        $c_j \la \textsc{Weight}(\gamma_j)$\;
    }
    Get optimal value $tmp$ and $b_1$, $\dots$, $b_i$ from \textsc{SensorPartition($i$, $c_1$, $\dots$, $c_i$, $m$)}\;
    \If{$tmp < res$}{
        $res \la tmp$\;
        Record all the $\gamma$'s and $b$'s\;
    }
    Delete the edge with the highest weight from $\mathcal{T}$, obtaining a forest with $i+1$ trees as the new $\mathcal{T}$\;
}
Output $res$ and the best $\gamma$'s and $b$'s respectively\;
\end{algorithm}

\subsection{Analysis: Time Complexity and Approximation Ratio}
The minimum spanning tree takes time $O(n \log n)$ if we use Prim's algorithm.

At each iteration, 
\begin{itemize}
	\item to get cycles from trees and calculate their weights take time $O(n)$ in total, if trees are represented using adjacent lists;
	\item \textsc{SensorPartition} takes time $O(km^2)$;
	\item to take the edge with the largest weight takes time $O(n)$ if we does not use other data structures to store edge weights;
	\item to get a new forest takes time $O(n)$ if we use \textsc{DFS}.
\end{itemize}
Therefore the total running time of this algorithm is $O(m(n+m^3))$.

To analyse its approximation ratio, consider the optimum trajectory schedule $\sigma = (k, \gamma_1, \dots, \gamma_k, b_1, \dots, b_k)$ and our approximated output trajectory schedule $\sigma^* = (k^*, \gamma^*_1$, $\dots, \gamma^*_{k^*}, b^*_1, \dots, b^*_{k^*})$. Let $c_i$ to be the weight of cycle $\gamma_i$ for $1 \le i \le k$ and $c^*_i$ of $\gamma^*_i$ for $1 \le i \le k^*$. Similarly, let $P_i$ be the vertices of cycle $\gamma_i$ and $P^*_i$ of $\gamma^*_i$. Let $MSTW(P)$ be the weight of the minimum spanning tree of $G\upharpoonright P$, the induced subgraph of $G$ on points from $P$.

For the optimum solution, we have 
\[cost(\sigma) = \max_{1 \le i \le k} \frac{c_i}{b_i} \ge \frac{1}{k} \sum_{i = 1}^k \frac{c_i}{b_i} \ge \frac{1}{m^2} \sum_{i = 1}^k c_i \ge \frac{1}{m^2} \sum_{i = 1}^k MSTW(P_i), \]
since the weight of cycle $\gamma_i$ must be larger than that of its corresponding minimum spanning tree (we can get a spanning tree by deleting an arbitrary edge in the cycle).

For the trajectory our algorithm outputs, we have
\[cost(\sigma^*) = \max_{1 \le i \le k^*} \frac{c^*_i}{b^*_i} \le \max_{1 \le i \le k^*} c^*_i \le 2 \max_{1 \le i \le k^*} MSTW(P^*_i), \]
since for each $i$ we have $c^*_i \le 2 MSTW(P^*_i)$ by the triangular inequality as in the proof of the $2$-approximation algorithm of \textsc{Metric-TSP}.

Let $D$ be the maximum distance between two target points, while $D'$ be the minimum one. Then 
\[\sum_{i = 1}^k MSTW(P_i) \ge (n-k) D' \ge (n-m)D'\]
since the forest formed by spanning trees of $P_i$'s contains $n-k$ edges and
\[\max_{1 \le i \le k^*} MSTW(P^*_i) \le MSTW(G) \le nD.\]
Therefore the approximation ratio is 
\[\frac{cost(\sigma^*)}{cost(\sigma)} \le \frac{2nD}{\frac{n-m}{m^2}D'} = O(m^2\frac{D}{D'}).\]

Notice that the approximation ratio doesn't depends on $n$, hence this algorithm may still perform well for very large $n$ and small $m$ and $\frac{D}{D'}$ although the bounds we give here for $cost(\sigma)$ and $cost(\sigma^*)$ are very loose. In an extreme case, if $m$ is constant and target points are given on the fixed size grid such that $\frac{D}{D'}$ is bounded by a constant then our algorithm has a constant approximation ratio.

\begin{Rem}
We believed that our approximation ratio can be further improved into $O(m^2n^2)$ which is a polynomial of input: if $D > \Omega(n^2) D'$ we may slightly move the points onto a grid then concentrate them, using a method similar to the preprocessing step of the PTAS of \textsc{Euclidean-TSP}\cite{vijay01}, so that the ratio of maximum and minimum distances can be controlled while the best trajectory schedule is preserved; then we can use the best trajectory schedule to compute its cost on the original graph. However we have not figured out how to make this improvement rigorously.
\end{Rem}
\begin{Rem}
This problem is hard to approximate and the approximation ratio is hard to analyse. The core of our algorithm is to obtain a tree cover of the graph. A slightly different version, \textsc{Min-Max Tree-Cover}\cite{even04}, is to find a tree cover such that the maximal weight of trees is minimized, which is equivalent to let all $b_i$'s to be $1$ in our setting. This problem is known to have an APX-hardness lower bound of $\frac{3}{2}$\cite{khani04}\cite{Xu10}.
\end{Rem}
\subsection{Experiments}
The following two method for solving \textsc{2-D CSC} are used as baselines in our experiments:
\begin{enumerate}
	\item[\emph{1}] \label{alg:base1}Compute a single TSP cycle, then split it into $m$ segments with the same length;
	\item[\emph{2}] \label{alg:base2}Partition the target points into $m$ clusters, then compute the TSP for each cycle respectively.
\end{enumerate}

In our implementation, the $2$-approximation algorithm for \textsc{Metric-TSP} is used for computing all the TSPs. Using our notation of algorithms in the previous sections, for the implementation of Baseline 1, we just output $\frac{1}{m}\textsc{Weight}(\textsc{MetricTSP}(\textsc{MST}(G)))$.

For Baseline 2 we use the standard $k$-means clustering algorithm\cite{John} to get $m$ clusters $\{P_1, \dots, P_m\}$. Then we calculate $\max_{1\le i \le m}\textsc{Weight}(\textsc{MetricTSP}(\textsc{MST}(G\upharpoonright P_i)))$ where $G\upharpoonright P_i$ is the induced subgraph of $G$ on points from $P_i$.

The main implementation codes are \verb!2D-code/baseline1.cpp! for Baseline 1 and \verb!2D-code/baseline2.cpp! for Baseline 2. However writing these two codes takes us too much effort and time so that we don't have enough time to implement our own algorithm and run simulations. We are so sorry not having empirical comparisons between our algorithm and baselines. However, we believe our algorithm will at least outperform this two baselines, since we already consider the TSP from the whole point set and when points are highly separated our method in the iteration with $k = m$ is equivalent to baseline $2$.

\section{Conclusion}
We develop an optimum algorithm for Cooperative Sweep Coverage problem on one-dimensional space, proving its correctness and showing some experimental results. For the case of two-dimensional space, we develop an approximation algorithm with approximation ratio $O(m^2\frac{D}{D'})$ where $D$ and $D'$ are the maximum and minimu, distance, respectively, between two target points, which we believed can be further improved and will perform well empirically. 

\begin{thebibliography}{9}
\bibitem{vijay01} Vazirani, Vijay V. \emph{Approximation Algorithms}. Springer Science \& Business Media, 2001.
\bibitem{clrs09} Cormen, Thomas H, et al. \emph{Introduction to algorithms}. MIT press, 2009.
\bibitem{even03} Even, Guy, et al.``Covering graphs using trees and stars." \emph{Approximation, Randomization, and Combinatorial Optimization}. \emph{Algorithms and Techniques}. Springer Berlin Heidelberg, 2003. 24-35.
\bibitem{even04} Even, Guy, et al. ``Min–max tree covers of graphs." \emph{Operations Research Letters} 32.4 (2004): 309-315.
\bibitem{khani04} Khani, M. Reza, and Mohammad R. Salavatipour. ``Improved approximation algorithms for the min-max tree cover and bounded tree cover problems." \emph{Algorithmica} 69.2 (2014): 443-460.
\bibitem{Xu10} Xu, Zhou, et al. ``Approximation hardness of min–max tree covers." \emph{Operations Research Letters} 38.3 (2010): 169-173.
\bibitem{John} Hopcroft, John E, et al. \emph{Computer Science Theory for the Information Age}. 2012.
\end{thebibliography}
\end{document}
